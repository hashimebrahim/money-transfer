# REST API for Money Transfer

Simple Money Transfer API to transfer money between users

### Technologies
- JAX-RS API
- CDI
- Bean Validation
- JUnit
- Thorntail


### How to run
```sh
mvn install
java -jar target/money-transfer-0.0.1-SNAPSHOT-thorntail.jar
```

Application starts a wildfly-swarm instance on port 8080. An In Memory DataStore is used save the data.

### Available Services

| HTTP METHOD | PATH | USAGE |
| -----------| ------ | ------ |
| GET | /accounts/{accountId} | Find an Account by AccountId | 
| GET | /accounts | Find All Accounts |
| GET | /accounts&page={pageNo}&limit={limit} | Find accounts with pagination |
| GET | /accounts/{accountId}/balance | Find balance of an account| 
| POST | /accounts | Create a New Account
| POST | /accounts/{accountId} | Update an already existing Account
| DELETE | /account/{accountId} | Delete an account by AccountId | 
| POST | /transfer | Transfer Money between source/destination |
| GET | /transfer/{accountId} | List of transactions of particular account | 

### Http Status
- 200 OK
- 400 Bad Request 
- 404 Not Found
- 500 Internal Server Error 

### Tests for Acceptance Test after running wildfly-swarm instance

Run using JUnit the following class of tests.
test.moneytransfer.service.AccountServiceTest.AccountServiceCheck 
test.moneytransfer.service.AccountServiceTest.TransferServiceCheck

