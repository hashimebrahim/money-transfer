package com.moneytransfer.validate;

import java.math.BigInteger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.moneytransfer.datastore.InMemoryDataStore;
import com.moneytransfer.model.Violation;
import com.moneytransfer.model.Violation.MessageTemplate;

@RequestScoped
public class CurrencyChecks {

	@Inject
	InMemoryDataStore datastore;

	public void check(BigInteger source, BigInteger destination, String currencyCode) {
		String sourceCurrency = datastore.findAccountById(source).getCurrency();
		String destinationCurrency = datastore.findAccountById(destination).getCurrency();
		if (!(sourceCurrency.equalsIgnoreCase(destinationCurrency) && sourceCurrency.equalsIgnoreCase(currencyCode))) {
			throw new Violation(MessageTemplate.CurrencyNotMatching);
		}

	}
}
