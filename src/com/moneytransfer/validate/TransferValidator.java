package com.moneytransfer.validate;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.moneytransfer.model.Transfer;

@RequestScoped
public class TransferValidator {
	@Inject
	private AccountChecks accountChecker;

	@Inject
	private AccountBalanceChecks balanceChecker;

	@Inject
	private CurrencyChecks currencyChecker;

	public void validate(Transfer transferDto) {
		accountChecker.checkSourceAccount(transferDto.getSourceAccountId());
		accountChecker.checkDestinationAccount(transferDto.getDestinationAccountId());
		balanceChecker.check(transferDto.getSourceAccountId(), transferDto.getAmount());
		currencyChecker.check(transferDto.getSourceAccountId(), transferDto.getDestinationAccountId(),
				transferDto.getCurrency());
	}
}
