package com.moneytransfer.validate;

import java.math.BigInteger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.moneytransfer.datastore.InMemoryDataStore;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.Violation;
import com.moneytransfer.model.Violation.MessageTemplate;

@RequestScoped
public class AccountChecks {

	@Inject
	private InMemoryDataStore datastore;

	public void checkSourceAccount(BigInteger sourceAccountId) {
		if (!checkAccountExists(sourceAccountId)) {
			throw new Violation(MessageTemplate.SourceAccountNotExists);
		}
	}

	public void checkDestinationAccount(BigInteger destinationAccountId) {
		if (!checkAccountExists(destinationAccountId)) {
			throw new Violation(MessageTemplate.DestinationAccountNotExists);
		}
	}

	private boolean checkAccountExists(BigInteger accountId) {
		Account account = datastore.findAccountById(accountId);
		return account != null ? true : false;
	}

}
