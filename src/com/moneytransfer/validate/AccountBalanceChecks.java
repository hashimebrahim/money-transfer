package com.moneytransfer.validate;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.moneytransfer.datastore.InMemoryDataStore;
import com.moneytransfer.model.Violation;
import com.moneytransfer.model.Violation.MessageTemplate;

@RequestScoped
public class AccountBalanceChecks {

	@Inject
	InMemoryDataStore datastore;

	public void check(BigInteger accountId, BigDecimal amountToTransfer) {
		BigDecimal balanceAmount = datastore.findAccountBalance(accountId);
		if (balanceAmount.compareTo(amountToTransfer) < 0) {
			throw new Violation(MessageTemplate.InsufficentBalance);
		}
	}
}
