package com.moneytransfer.business;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.moneytransfer.datastore.InMemoryDataStore;
import com.moneytransfer.model.Transfer;

@RequestScoped
public class TransferFund {

	@Inject
	private InMemoryDataStore datastore;

	public Transfer transfer(Transfer transferDto){
		return datastore.makeTransfer(transferDto);
	}
}
