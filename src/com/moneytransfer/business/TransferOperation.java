package com.moneytransfer.business;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.moneytransfer.model.Transfer;
import com.moneytransfer.validate.TransferValidator;

@RequestScoped
public class TransferOperation {

	@Inject
	TransferValidator validator;
	
	@Inject
	TransferFund fundTransfer;
	
	public Transfer transfer(Transfer transferDto) {
		validator.validate(transferDto);
		transferDto = fundTransfer.transfer(transferDto);
		return transferDto;
	}

}
