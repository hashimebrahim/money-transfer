package com.moneytransfer.service;

import java.math.BigInteger;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.moneytransfer.business.TransferOperation;
import com.moneytransfer.datastore.InMemoryDataStore;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.Transfer;
import com.moneytransfer.model.Transfers;
import com.moneytransfer.model.Violation;

@Path(value = "/transfer")
public class TransferService {

	@Inject
	private TransferOperation transferOp;

	@Inject
	private InMemoryDataStore datastore;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response transferAmount(@Valid Transfer transferDto) {
		try {
			transferDto = transferOp.transfer(transferDto);
		} catch (Violation violation) {
			return handleException(violation);
		}
		return Response.ok(transferDto, MediaType.APPLICATION_JSON).build();
	}

	@GET
	@Path("{accountId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findTransferByAccountId(@PathParam("accountId") BigInteger accountId) {
		Account account = datastore.findAccountById(accountId);
		if (account == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		List<Transfer> transfers = datastore.findTransactionByAccountId(accountId);
		return Response.status(Status.OK).entity(new Transfers(transfers)).build();
	}

	private Response handleException(Violation violation) {
		switch (violation.getViolationCause()) {
		case SourceAccountNotExists:
			return Response.status(Status.NOT_FOUND).entity(violation.getMessage()).build();
		case DestinationAccountNotExists:
			return Response.status(Status.NOT_FOUND).entity(violation.getMessage()).build();
		case CurrencyNotMatching:
			return Response.status(Status.BAD_REQUEST).entity(violation.getMessage()).build();
		case InsufficentBalance:
			return Response.status(Status.BAD_REQUEST).entity(violation.getMessage()).build();

		default:
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
