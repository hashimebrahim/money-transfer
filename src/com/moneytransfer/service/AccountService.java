package com.moneytransfer.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.moneytransfer.datastore.InMemoryDataStore;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.Accounts;

@Path("/accounts")
public class AccountService {

	@Inject
	private InMemoryDataStore datastore;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAllAccounts(@QueryParam("page") int page, @QueryParam("limit") int limit) {
		// TODO Move to business-layer.
		if ((page < 0 || limit < 0) || (page == 0 && limit > 0)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		List<Account> accounts = datastore.findAccount(page, limit);
		return Response.status(Status.OK).entity(new Accounts(accounts)).build();
	}

	@GET
	@Path("{accountId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAccountById(@PathParam("accountId") BigInteger accountId) {
		Account account = datastore.findAccountById(accountId);
		if (account == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).entity(datastore.findAccountById(accountId)).build();
	}

	@GET
	@Path("{accountId}/balance")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAccountBalanceById(@PathParam("accountId") String accountId) {
		BigDecimal accountBalance = datastore.findAccountBalance(new BigInteger(accountId));
		if (accountBalance == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).entity(accountBalance).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createNewAccount(@Valid Account newAccount) {
		newAccount = datastore.createAccount(newAccount);
		return Response.status(Status.OK).entity(newAccount).build();
	}

	@PUT
	@Path("{accountId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateAccount(@PathParam("accountId") String accountId, Account accountToBeUpdated) {

		if (datastore.findAccountById(new BigInteger(accountId)) == null) {
			Response.status(Status.NOT_FOUND).build();
		}

		accountToBeUpdated.setAccountNo(new BigInteger(accountId));
		Account updatedAccount = datastore.updateAccount(accountToBeUpdated);
		if (updatedAccount == null) {
			Response.status(Status.NOT_FOUND).build();
		}

		return Response.status(Status.OK).entity(updatedAccount).build();
	}

	@DELETE
	@Path("{accountId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteAccount(@PathParam("accountId") String accountId) {

		BigInteger accountIdToBeRemoved = new BigInteger(accountId);
		if (datastore.findAccountById(accountIdToBeRemoved) == null) {
			Response.status(Status.NOT_FOUND).build();
		}

		accountIdToBeRemoved = datastore.deleteAccount(accountIdToBeRemoved);
		if (accountIdToBeRemoved == null) {
			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok().build();
	}

}