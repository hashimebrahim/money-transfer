package com.moneytransfer.model;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

public class Transfer {
	@Null
	private BigInteger id;
	@NotNull(message = "Source Account Id must be provided")
	private BigInteger sourceAccountId;
	@NotNull(message = "Destination Account Id must be provided")
	private BigInteger destinationAccountId;
	@NotNull(message = "Amount must be provided")
	private BigDecimal amount;
	@NotNull(message = "Currency Code must be provided")
	private String currency;
	private String remarks;
	@Null
	private TransferStatus status;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getSourceAccountId() {
		return sourceAccountId;
	}

	public void setSourceAccountId(BigInteger sourceAccountId) {
		this.sourceAccountId = sourceAccountId;
	}

	public BigInteger getDestinationAccountId() {
		return destinationAccountId;
	}

	public void setDestinationAccountId(BigInteger destinationAccountId) {
		this.destinationAccountId = destinationAccountId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public TransferStatus getStatus() {
		return status;
	}

	public void setStatus(TransferStatus status) {
		this.status = status;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
