package com.moneytransfer.model;

import java.util.List;

public class Transfers {
	private List<Transfer> transfers;
	
	public Transfers() {
	}
	
	public Transfers(List<Transfer> transfers){
		this.transfers = transfers;
	}

	public List<Transfer> getTransfers() {
		return transfers;
	}

	public void setTransfers(List<Transfer> transfers) {
		this.transfers = transfers;
	}
	
}
