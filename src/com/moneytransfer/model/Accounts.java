package com.moneytransfer.model;

import java.util.List;

public class Accounts {
	List<Account> accounts;
	
	public Accounts() {
	}
	
	public Accounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
	
}
