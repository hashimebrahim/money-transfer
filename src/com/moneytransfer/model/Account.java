package com.moneytransfer.model;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

public class Account {
	@Null
	private BigInteger accountNo;
	@NotNull
	private String name;

	private BigDecimal balance = BigDecimal.ZERO;
	// ISO 4217 Currency Code
	@NotNull
	private String currency;

	public Account() {
	}

	public Account(BigInteger accountNo, String name, BigDecimal balance, String currency) {
		super();
		this.accountNo = accountNo;
		this.name = name;
		this.balance = balance;
		this.currency = currency;
	}

	public BigInteger getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(BigInteger accountNo) {
		this.accountNo = accountNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
