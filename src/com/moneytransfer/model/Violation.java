package com.moneytransfer.model;

public class Violation extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private MessageTemplate violationCause;

	public Violation(MessageTemplate violationCause) {
		this.violationCause = violationCause;
	}

	public MessageTemplate getViolationCause() {
		return violationCause;
	}

	public void setViolationCause(MessageTemplate violationCause) {
		this.violationCause = violationCause;
	}

	@Override
	public String getMessage() {
		if (violationCause != null) {
			return violationCause.value();
		}
		return "";
	}

	public enum MessageTemplate {
		SourceAccountNotExists("Sender Account is not Existing"), DestinationAccountNotExists(
				"Receiver Account is not Existing"), InsufficentBalance(
						"Source Account does not have sufficent Balance"), CurrencyNotMatching(
								"This currency transfer is not possible with current sender/receiver");

		private final String value;

		MessageTemplate(String v) {
			value = v;
		}

		public String value() {
			return value;
		}

		public static MessageTemplate fromValue(String v) {
			for (MessageTemplate c : MessageTemplate.values()) {
				if (c.value.equals(v)) {
					return c;
				}
			}
			throw new IllegalArgumentException(v);
		}
	}
}
