package com.moneytransfer.datastore;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import com.moneytransfer.model.Account;
import com.moneytransfer.model.Transfer;
import com.moneytransfer.model.TransferStatus;

@ApplicationScoped
public class InMemoryDataStore implements AccountDAO, TransferDAO {

	private List<Account> accounts;
	private Map<BigInteger, Transfer> transfers;

	@PostConstruct
	public void initialize() {
		accounts = new ArrayList<Account>();
		accounts.add(new Account(getNextAccountId(), "John", BigDecimal.valueOf(100.0), "EUR"));
		accounts.add(new Account(getNextAccountId(), "Joseph", BigDecimal.valueOf(50.0), "EUR"));
		accounts.add(new Account(getNextAccountId(), "Thomas", BigDecimal.valueOf(25.0), "EUR"));
		accounts.add(new Account(getNextAccountId(), "Jose", BigDecimal.valueOf(5.0), "EUR"));
		accounts.add(new Account(getNextAccountId(), "Mathew", BigDecimal.valueOf(0.0), "EUR"));

		transfers = new HashMap<BigInteger, Transfer>();
	}

	@Override
	public List<Account> findAccount(int page, int limit) {
		if (page == 0 && limit == 0) {
			return accounts;
		}

		List<Account> results = accounts.stream().skip((page - 1) * limit).limit(limit).collect(Collectors.toList());

		return results;
	}

	@Override
	public Account findAccountById(BigInteger accountId) {
		return accounts.stream().filter(account -> accountId.equals(account.getAccountNo())).findAny().orElse(null);
	}

	public BigDecimal findAccountBalance(BigInteger accountId) {
		Account account = accounts.stream().filter(account1 -> accountId.equals(account1.getAccountNo())).findAny()
				.orElse(null);
		if (account == null) {
			return null;
		}
		return account.getBalance();
	}

	@Override
	public Account createAccount(Account newAccount) {
		newAccount.setAccountNo(getNextAccountId());
		accounts.add(newAccount);
		return newAccount;
	}

	@Override
	public Account updateAccount(Account accountToBeUpdated) {
		Account account = accounts.stream()
				.filter(account2 -> accountToBeUpdated.getAccountNo().equals(account2.getAccountNo())).findAny()
				.orElse(null);
		if (account == null) {
			return null;
		}

		if (accountToBeUpdated.getBalance() != null) {
			account.setBalance(accountToBeUpdated.getBalance());
		}
		if (accountToBeUpdated.getName() != null) {
			account.setName(accountToBeUpdated.getName());
		}
		if (accountToBeUpdated.getCurrency() != null) {
			account.setCurrency(accountToBeUpdated.getCurrency());
		}

		return account;

	}

	@Override
	public BigInteger deleteAccount(BigInteger accountToBeUpdated) {
		boolean isFound = false;
		Account accountToFind = new Account();
		for (Account eachAccount : this.accounts) {
			if (eachAccount.getAccountNo().equals(accountToBeUpdated)) {
				isFound = true;
				accountToFind = eachAccount;
				break;
			}
		}
		if (isFound) {
			this.accounts.remove(accountToFind);
		}
		return isFound ? accountToBeUpdated : null;
	}

	public BigInteger getNextAccountId() {
		if (this.accounts == null || this.accounts.size() == 0) {
			return BigInteger.ONE;

		}
		Account account = this.accounts.stream().max(Comparator.comparing(Account::getAccountNo)).orElse(new Account());
		if (account.getAccountNo() == null) {
			return BigInteger.ONE;
		}
		return account.getAccountNo().add(BigInteger.ONE);
	}

	public Transfer makeTransfer(Transfer transferDto) {
		BigInteger key = getNextTransferLogId();
		try {
			Account sourceAccount = findAccountById(transferDto.getSourceAccountId());
			Account destinationAccount = findAccountById(transferDto.getDestinationAccountId());

			destinationAccount.setBalance(destinationAccount.getBalance().add(transferDto.getAmount()));
			sourceAccount.setBalance(sourceAccount.getBalance().subtract(transferDto.getAmount()));

			updateAccount(sourceAccount);
			updateAccount(destinationAccount);

			transferDto.setId(key);

			transferDto.setStatus(TransferStatus.SUCCESS);
		} catch (Exception E) {
			transferDto.setStatus(TransferStatus.FAILED);
		} finally {
			transfers.put(key, transferDto);
		}
		return transferDto;
	}

	public List<Transfer> findTransactionByAccountId(BigInteger accountId) {
		List<Transfer> transfersOfThisAccount = this.transfers.entrySet().stream()
				.filter(e -> e.getValue().getSourceAccountId().equals(accountId)).map(Map.Entry::getValue)
				.collect(Collectors.toList());
		return transfersOfThisAccount;
	}

	public BigInteger getNextTransferLogId() {
		if (this.transfers == null || this.transfers.size() == 0) {
			return BigInteger.ONE;

		}

		BigInteger nextTransferLogId = this.transfers.keySet().stream().max(BigInteger::compareTo)
				.orElse(BigInteger.ZERO);
		return nextTransferLogId.add(BigInteger.ONE);

	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	public Map<BigInteger, Transfer> getTransfers() {
		return transfers;
	}

	public void setTransfers(Map<BigInteger, Transfer> transfers) {
		this.transfers = transfers;
	}
}
