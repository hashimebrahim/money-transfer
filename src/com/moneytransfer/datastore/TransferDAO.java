package com.moneytransfer.datastore;

import java.math.BigInteger;
import java.util.List;

import com.moneytransfer.model.Transfer;

public interface TransferDAO {

	public Transfer makeTransfer(Transfer transferDto);

	public List<Transfer> findTransactionByAccountId(BigInteger accountId);

}
