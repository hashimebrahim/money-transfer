package com.moneytransfer.datastore;

import java.math.BigInteger;
import java.util.List;

import com.moneytransfer.model.Account;

public interface AccountDAO {

	public List<Account> findAccount(int page, int limit);

	public Account findAccountById(BigInteger accountId);

	public Account createAccount(Account newAccount);

	public Account updateAccount(Account accountToBeUpdated);

	public BigInteger deleteAccount(BigInteger accountToBeUpdated);

}
