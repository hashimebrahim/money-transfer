package test.moneytransfer.service.AccountServiceTest;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.moneytransfer.model.Account;
import com.moneytransfer.model.Accounts;

/**
 * @author Hashim.Ebrahim Tests while running the container on port 8080
 *
 */
public class AccountServiceCheck {

	private static final String REST_URI = "http://localhost:8080";

	private static Client client;

	@BeforeClass
	public static void init() {
		client = ClientBuilder.newClient();
	}

	/**
	 * Test to fetch all accounts without any pagination
	 * http://localhost:8080/accounts
	 */
	@Test
	public void find_all_accounts() {
		Accounts accounts = (Accounts) client.target(REST_URI + "/accounts").request(MediaType.APPLICATION_JSON)
				.get(Accounts.class);
		Assert.assertNotNull(accounts.getAccounts());
	}

	/**
	 * Test to fetch all accounts with pagination
	 * http://localhost:8080/accounts?page=4&limit=1
	 */
	@Test
	public void find_paginated_accounts() {
		String url = REST_URI + "/accounts?limit=1&page=4";
		System.out.println(url);
		Accounts accounts = (Accounts) client.target(url).request(MediaType.APPLICATION_JSON).get(Accounts.class);

		Assert.assertEquals(accounts.getAccounts().size(), 1);

		Account account = accounts.getAccounts().get(0);
		Assert.assertEquals(account.getName(), "Jose");
	}

	/**
	 * Test to fetch an details of a specific account.
	 * http://localhost:8080/accounts/{accountId}
	 */
	@Test
	public void find_account_by_id() {
		String url = REST_URI + "/accounts/5";
		Account accountDetails = client.target(url).request(MediaType.APPLICATION_JSON).get(Account.class);
		Assert.assertEquals(accountDetails.getName(), "Mathew");

	}

	/**
	 * Test to fetch the balance of a specific account. GET
	 * http://localhost:8080/accounts/{accountId}/balance
	 */
	@Test
	public void find_account_balance() {
		String url = REST_URI + "/accounts/4/balance";
		BigDecimal balance = client.target(url).request(MediaType.APPLICATION_JSON).get(BigDecimal.class);

		Assert.assertEquals(balance, BigDecimal.valueOf(5.0));
	}

	/**
	 * Test to create an account. POST http://localhost:8080/accounts
	 */
	@Test
	public void create_account() {
		String url = REST_URI + "/accounts";

		Account account = new Account(null, "Adam", BigDecimal.valueOf(0.0), "EUR");
		Response response = client.target(url).request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(account, MediaType.APPLICATION_JSON));

		Assert.assertEquals(response.getStatus(), 200);
	}

	/**
	 * Test to update an account. PUT http://localhost:8080/accounts/{accountId}
	 */
	@Test
	public void update_account_with_name() {
		String url = REST_URI + "/accounts/1";

		Account account = new Account(BigInteger.valueOf(1), "Noah", BigDecimal.valueOf(0.0), "EUR");
		Response response = client.target(url).request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(account, MediaType.APPLICATION_JSON));

		Assert.assertEquals(response.getStatus(), 200);

		// Call the API to make sure name is updated.
		String url2 = REST_URI + "/accounts/1";
		Account accountDetails = client.target(url2).request(MediaType.APPLICATION_JSON).get(Account.class);
		Assert.assertEquals(accountDetails.getName(), "Noah");
	}

	/**
	 * Test to delete an account. DELETE
	 * http://localhost:8080/accounts/{accountId}
	 * 
	 * This test will delete the entry located at 3.
	 */
	@Test(expected=NotFoundException.class)
	public void delete_an_account() {
		String url = REST_URI + "/accounts/3";

		Response response = client.target(url).request(MediaType.APPLICATION_JSON).delete();
		Assert.assertEquals(response.getStatus(), 200);

		String url2 = REST_URI + "/accounts/3";
		System.out.println(url2);
		client.target(url2).request(MediaType.APPLICATION_JSON).get(Account.class);
	}
	
	/**
	 * Test to validate field rules while creating an account 
	 * POST 
	 * 		http://localhost:8080/accounts
	 */
	@Test
	public void validate_mandatory_fields_while_posting() {
		String url = REST_URI + "/accounts";

		Account account = new Account(null, null, null, null);
		
		Response response = client.target(url).request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(account, MediaType.APPLICATION_JSON));
		
		//Mandatory field: Name/CurrencyCode
		
		Assert.assertEquals(response.getStatus(), 400);
	}

}
