package test.moneytransfer.service.AccountServiceTest;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.moneytransfer.model.Account;
import com.moneytransfer.model.Transfer;

public class TransferServiceCheck {

	private static final String REST_URI = "http://localhost:8080";

	private static Client client;

	@BeforeClass
	public static void init() {
		client = ClientBuilder.newClient();
	}

	/**
	 * Test to transfer an amount of 10 EUR from 1st account to 2nd account
	 * POST http://localhost:8080/transfer
	 */
	@Test
	public void transfer_amount_from_second_acc_to_fifth_acc() {
		doATransferBetweenAccount();

		// Check whether amount has been updated or not.
		// Initial balance of 5th = 0, after update it should be 5.
		String url = REST_URI + "/accounts/5";
		Account accountDetails = client.target(url).request(MediaType.APPLICATION_JSON).get(Account.class);
		
		// it should be more than 0 as initial value is 0.
		Assert.assertNotEquals(accountDetails.getBalance(), BigDecimal.valueOf(0.0));

	}
	
	
	/**
	 * Test to validate a transfer if source account does not exists.
	 * POST http://localhost:8080/transfer
	 */
	@Test
	public void validate_transfer_source_account_not_existing() {
		
		Transfer transferDto = new Transfer();
		transferDto.setSourceAccountId(BigInteger.valueOf(100));
		transferDto.setDestinationAccountId(BigInteger.valueOf(2));
		transferDto.setAmount(BigDecimal.valueOf(10));
		transferDto.setCurrency("EUR");
		transferDto.setRemarks("Some Comment");
		
		Response transferResponse = client.target(REST_URI + "/transfer").request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(transferDto, MediaType.APPLICATION_JSON));
		Assert.assertEquals(transferResponse.getStatus(), 404);
	}
	
	/**
	 * Test to validate a transfer if destination account does not exists.
	 * POST http://localhost:8080/transfer
	 */
	@Test
	public void validate_transfer_destination_account_not_existing() {
		
		Transfer transferDto = new Transfer();
		transferDto.setSourceAccountId(BigInteger.valueOf(1));
		transferDto.setDestinationAccountId(BigInteger.valueOf(200));
		transferDto.setAmount(BigDecimal.valueOf(10));
		transferDto.setCurrency("EUR");
		transferDto.setRemarks("Some Comment");
		
		Response transferResponse = client.target(REST_URI + "/transfer").request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(transferDto, MediaType.APPLICATION_JSON));
		Assert.assertEquals(transferResponse.getStatus(), 404);
	}
	
	/**
	 * Test to validate a transfer if currency not matching with source & destination.
	 * POST 
	 * 		http://localhost:8080/transfer
	 */
	@Test
	public void validate_transfer_currency_not_matching() {
		
		Transfer transferDto = new Transfer();
		transferDto.setSourceAccountId(BigInteger.valueOf(1));
		transferDto.setDestinationAccountId(BigInteger.valueOf(2));
		transferDto.setAmount(BigDecimal.valueOf(10));
		transferDto.setCurrency("SAR");
		transferDto.setRemarks("Some Comment");
		
		Response transferResponse = client.target(REST_URI + "/transfer").request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(transferDto, MediaType.APPLICATION_JSON));
		Assert.assertEquals(transferResponse.getStatus(), 400);
	}
	
	/**
	 * Test to validate a transfer if source account does not have enough balance
	 * POST 
	 * 		http://localhost:8080/transfer
	 */
	@Test
	public void validate_transfer_insufficent_balance() {
		
		Transfer transferDto = new Transfer();
		transferDto.setSourceAccountId(BigInteger.valueOf(5));
		transferDto.setDestinationAccountId(BigInteger.valueOf(2));
		transferDto.setAmount(BigDecimal.valueOf(10));
		transferDto.setCurrency("SAR");
		transferDto.setRemarks("Some Comment");
		
		Response transferResponse = client.target(REST_URI + "/transfer").request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(transferDto, MediaType.APPLICATION_JSON));
		Assert.assertEquals(transferResponse.getStatus(), 400);
	}
	
	/**
	 * Test to validate a the field rules
	 * POST 
	 * 		http://localhost:8080/transfer
	 */
	@Test
	public void validate_field_rules_for_mandatory_fields() {
		
		Transfer transferDto = new Transfer();
		transferDto.setRemarks("Some Comment");
		
		Response transferResponse = client.target(REST_URI + "/transfer").request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(transferDto, MediaType.APPLICATION_JSON));
		
		// Mandatory feilds : Source/Destination/Amount/Currency
		Assert.assertEquals(transferResponse.getStatus(), 400);
	}

	/**
	 * Test to find all transactions of a particular account
	 * POST 
	 * 		http://localhost:8080/transfer/{accountId}
	 */
	@Test
	public void find_all_transactions_for_account_2() {
		doATransferBetweenAccount();

		// Check whether amount has been updated or not.
		// Initial balance of 2nd = 50, after update it should be 60.
		String url = REST_URI + "/accounts/2";
		Account accountDetails = client.target(url).request(MediaType.APPLICATION_JSON).get(Account.class);
		// It should not same as initial value, as 50.
		Assert.assertNotEquals(accountDetails.getBalance(), BigDecimal.valueOf(50.0));
	}
	
	private void doATransferBetweenAccount() {
		Transfer transferDto = new Transfer();
		transferDto.setSourceAccountId(BigInteger.valueOf(2));
		transferDto.setDestinationAccountId(BigInteger.valueOf(5));
		transferDto.setAmount(BigDecimal.valueOf(5));
		transferDto.setCurrency("EUR");
		transferDto.setRemarks("Some Comment");

		Response transferResponse = client.target(REST_URI + "/transfer").request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(transferDto, MediaType.APPLICATION_JSON));
		Assert.assertEquals(transferResponse.getStatus(), 200);
	}

}
