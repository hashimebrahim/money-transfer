package test.moneytransfer.datastore;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.moneytransfer.datastore.InMemoryDataStore;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.Transfer;

public class InMemoryDataStoreTest {
	private InMemoryDataStore datastore;

	@Before
	public void before() {
		datastore = new InMemoryDataStore();

	}

	@Test
	public void test_get_all_accounts() {
		datastore.initialize();
		List<Account> accounts = datastore.findAccount(0, 0);
		Assert.assertEquals(accounts.size(), 5);
	}

	@Test
	public void test_get_account_paginated_input() {
		datastore.initialize();
		List<Account> accounts = datastore.findAccount(1, 2);
		Assert.assertEquals(accounts.size(), 2);
		Assert.assertEquals(accounts.get(0).getName(), "John");
	}

	@Test
	public void get_next_account_id_after_datastore_initialize() {
		datastore.initialize();
		BigInteger nextAccountId = datastore.getNextAccountId();
		Assert.assertEquals(nextAccountId, BigInteger.valueOf(6));
	}

	@Test
	public void get_next_account_id_before_datastore_initialize() {
		BigInteger nextAccountId = datastore.getNextAccountId();
		Assert.assertEquals(nextAccountId, BigInteger.valueOf(1));
	}

	@Test
	public void update_account_with_no_account_existing() {
		datastore.initialize();
		Account updatingAcc = new Account(BigInteger.TEN, "H", BigDecimal.valueOf(0.0), "EUR");
		Account updatedAccount = datastore.updateAccount(updatingAcc);
		Assert.assertNull(updatedAccount);

	}

	@Test
	public void update_account_with_name_account_existing() {
		datastore.initialize();
		Account updatingAcc = new Account(BigInteger.valueOf(4), "H", BigDecimal.valueOf(1.0), "EUR");
		Account updatedAccount = datastore.updateAccount(updatingAcc);
		Assert.assertEquals(updatedAccount.getBalance(), BigDecimal.valueOf(1.0));
		Assert.assertEquals(updatedAccount.getName(), "H");
		Assert.assertEquals(updatedAccount.getCurrency(), "EUR");
	}

	@Test
	public void get_next_transfer_id_for_empty_list() {
		datastore.initialize();
		Assert.assertEquals(datastore.getNextTransferLogId(), BigInteger.ONE);
	}

	@Test
	public void get_next_transfer_id_for_non_empty_list() {
		Transfer dto = new Transfer();
		dto.setAmount(BigDecimal.valueOf(10));
		dto.setCurrency("EUR");
		dto.setDestinationAccountId(BigInteger.ONE);
		dto.setRemarks("Rent");
		dto.setSourceAccountId(BigInteger.TEN);

		datastore.initialize();
		datastore.getTransfers().put(BigInteger.ONE, dto);
		datastore.getTransfers().put(BigInteger.valueOf(2), dto);
		Assert.assertEquals(datastore.getNextTransferLogId(), BigInteger.valueOf(3));

	}
	
	@Test
	public void get_transfers_for_an_account(){
		datastore.initialize();
		
		Transfer dto = new Transfer();
		dto.setAmount(BigDecimal.valueOf(10));
		dto.setCurrency("EUR");
		dto.setDestinationAccountId(BigInteger.ONE);
		dto.setRemarks("Rent");
		dto.setSourceAccountId(BigInteger.valueOf(2));
		datastore.makeTransfer(dto);
		
		List<Transfer> transfers = datastore.findTransactionByAccountId(BigInteger.valueOf(2));
		Assert.assertEquals(transfers.size(), 1);
		
		
	}
	
	@Test
	public void get_transfers_for_a_non_transfer_account(){
		datastore.initialize();
		
		Transfer dto = new Transfer();
		dto.setAmount(BigDecimal.valueOf(10));
		dto.setCurrency("EUR");
		dto.setDestinationAccountId(BigInteger.ONE);
		dto.setRemarks("Rent");
		dto.setSourceAccountId(BigInteger.valueOf(2));
		datastore.makeTransfer(dto);
		
		List<Transfer> transfers = datastore.findTransactionByAccountId(BigInteger.valueOf(1));
		Assert.assertEquals(transfers.size(), 0);
		
		
	}
}